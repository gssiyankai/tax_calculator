package controllers

/**
  * Enumeration of good categories
  */
object Category extends Enumeration {
  val BOOK            = Value("BOOK")
  val FOOD            = Value("FOOD")
  val MEDICAL_PRODUCT = Value("MEDICAL_PRODUCT")
  val OTHER          = Value("OTHER")
}

/**
  * Enumeration of good origin
  */
object Origin extends Enumeration {
  val LOCAL = Value("LOCAL")
  val IMPORT = Value("IMPORT")
}

/**
  * Case class representing a good
  */
case class Good(description: String, category: Category.Value, origin: Origin.Value, priceInCents: Long)

/**
  * Entry point to compute sales tax to apply on a good
  */
object TaxCalculator {

  val BASIC_TAX_RATE = 10
  val IMPORT_TAX_RATE = 5

  /**
    * Compute the sales tax
    * @param good the good on which to apply tax
    * @return the tax
    */
  def salesTaxInCents(good: Good): Long = {
    val basicTaxRate = good.category match {
      case Category.BOOK | Category.FOOD | Category.MEDICAL_PRODUCT => 0
      case Category.OTHER => BASIC_TAX_RATE
    }
    val importTaxRate = good.origin match {
      case Origin.LOCAL => 0
      case Origin.IMPORT => IMPORT_TAX_RATE
    }
    val tax = (basicTaxRate + importTaxRate) * good.priceInCents
    roundUp(tax / 100)
  }

  /**
    * Round up a tax in cents to the nearest 0.05 amount
    * @param tax the double value to round up
    * @return the rounded value
    */
  def roundUp(tax: Long): Long = {
    val cents = tax % 10
    if(cents == 0) {
      tax
    } else if(cents <= 5) {
      tax - cents + 5
    } else {
      tax + 10 - cents
    }
  }

}
