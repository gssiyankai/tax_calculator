package controllers

import javax.inject._

import play.api.Logger
import play.api.libs.json._
import play.api.mvc._

/**
  * This controller creates an `Action` to handle HTTP requests to the application's tax calculator.
  */
@Singleton
class TaxCalculatorController @Inject() extends Controller {

  /**
    * Action to compute sales tax on goods sent in request
    */
  def process = Action(parse.json) { request =>
    unmarshalJsValue(request) { resources: List[GoodResource] =>
      Logger.info(s"Received $resources")

      val tax = TaxCalculator.roundUp(
        resources.foldLeft(0L) { case (acc, r) =>
          acc +
            r.count *
              TaxCalculator.salesTaxInCents(
                Good(
                  r.description,
                  Category.withName(r.category.toUpperCase),
                  if (r.imported) Origin.IMPORT else Origin.LOCAL,
                  (r.unitPrice * 100).toLong
                )
              )
        }
      )

      Logger.info(s"Computed tax in cents = $tax")

      Ok(Json.toJson(TaxResource(tax / 100.0)))
    }
  }

  private def unmarshalJsValue[R](request: Request[JsValue])(block: R => Result)(implicit rds : Reads[R]): Result =
    request.body.validate[R](rds).fold(
      valid = block,
      invalid = e => {
        val error = e.mkString
        Logger.error(error)
        BadRequest(error)
      }
    )

}

/**
  * Case class representing a good sent in post request
  */
case class GoodResource(description: String, category: String, imported: Boolean, count: Long, unitPrice: Double)

/**
  * Companion object on good resource to declare implicit conversion from JSON
  */
object GoodResource {
  implicit val reads: Reads[GoodResource] = Json.reads[GoodResource]
}

/**
  * Case class representing the sales tax sent back in post reply
  */
case class TaxResource(salesTax: Double)

/**
  * Companion object on tax resource to declare implicit conversion to JSON
  */
object TaxResource {
  implicit val writes: Writes[TaxResource] = Json.writes[TaxResource]
}
