package controllers

import controllers.Category._
import controllers.Origin._
import controllers.TaxCalculator.salesTaxInCents
import org.scalatest.FlatSpec

/**
 * Specification of Tax Calculator
 */
class TaxCalculatorSpec extends FlatSpec {

  it should "exist 4 categories of goods: FOOD, BOOK, MEDICAL_PRODUCT and OTHER" taggedAs UnitTest in {
    List("FOOD", "BOOK", "MEDICAL_PRODUCT", "OTHER").foreach { c =>
      assert(Category.values.exists(_.toString == c))
    }
    assert(Category.values.size === 4)
  }

  it should "apply a tax rate of 10% except books, food and medical products" taggedAs UnitTest in {
    assert(salesTaxInCents(aGood(BOOK, LOCAL)) === 0)
    assert(salesTaxInCents(aGood(FOOD, LOCAL)) === 0)
    assert(salesTaxInCents(aGood(MEDICAL_PRODUCT, LOCAL)) === 0)
    assert(salesTaxInCents(aGood(OTHER, LOCAL)) === 10)
  }

  it should "apply an additional tax rate of 5% on all imported goods" taggedAs UnitTest in {
    assert(salesTaxInCents(aGood(BOOK, IMPORT)) === 5)
    assert(salesTaxInCents(aGood(FOOD, IMPORT)) === 5)
    assert(salesTaxInCents(aGood(MEDICAL_PRODUCT, IMPORT)) === 5)
    assert(salesTaxInCents(aGood(OTHER, IMPORT)) === 15)
  }

  it should "round the sales tax up to the nearest 5 cents" taggedAs UnitTest in {
    assert(salesTaxInCents(aGood(OTHER, LOCAL, 1499)) === 150)
    assert(salesTaxInCents(aGood(OTHER, IMPORT, 4750)) === 715)
    assert(salesTaxInCents(aGood(FOOD, IMPORT, 1125)) === 60)
  }

  def aGood(category: Category.Value, origin: Origin.Value): Good = aGood(category, origin, 100)
  def aGood(category: Category.Value, origin: Origin.Value, priceInCents: Long): Good = Good("Item", category, origin, priceInCents)

}
