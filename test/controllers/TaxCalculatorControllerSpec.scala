package controllers

import akka.stream.Materializer
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.{Json, Writes}
import play.api.test.Helpers._
import play.api.test._

class TaxCalculatorControllerSpec extends PlaySpec with GuiceOneAppPerSuite {

  implicit lazy val materializer: Materializer = app.materializer
  implicit val writes: Writes[GoodResource] = Json.writes[GoodResource]

  "TaxCalculatorController POST" should {

    "Compute the sales tax of a list of items" taggedAs IntegrationTest in {
      val controller = new TaxCalculatorController

      val configuration = List(
        (
          1.5,
          List(
            GoodResource("Book", "book", imported = false, 1, 12.49),
            GoodResource("Chocolate Bar", "food", imported = false, 1, 0.85),
            GoodResource("Music CD", "other", imported = false, 1, 14.99)
          )
        ),
        (
          7.65,
          List(
            GoodResource("Box of chocolates", "food", imported = true, 1, 10.0),
            GoodResource("Bottle of perfume", "other", imported = true, 1, 47.50)
          )
        ),
        (
          6.7,
          List(
            GoodResource("Bottle of perfume", "other", imported = true, 1, 27.99),
            GoodResource("Bottle of perfume", "other", imported = false, 1, 18.99),
            GoodResource("Packet of headache pills", "medical_product", imported = false, 1, 9.75),
            GoodResource("Box of chocolates", "food", imported = true, 1, 11.25)
          )
        )
      )

      configuration.foreach { case (expectedTax, goods) =>

        val request = FakeRequest("POST", "/taxcalculator").withJsonBody(Json.toJson(goods))

        val result = call(controller.process(), request)

        status(result) mustBe OK
        contentType(result) mustBe Some("application/json")
        contentAsJson(result) mustBe Json.toJson(TaxResource(expectedTax))

      }
    }

  }
}
