# Tax Calculator #

This implements in Scala with Play Framework 2.5 a restful API that computes the sales tax on goods according to the 3 following rules:

* Basic sales tax is applicable at a rate of 10% on all goods, except books, food, and medical products that are exempt.
* Import duty is an additional sales tax applicable on all imported goods at a rate of 5%, with no exemptions.
* Given a tax rate of n% and price p the tax is n*p/100 rounded up to the nearest 0.05 amount. It is therefore assumed that the rounding up is done on each sales tax of each good separately and not after summing up sales taxes of all goods. 

Please note that a significant modification has been added to the data model given in the initial description of the coding challenge. It is mandatory that the client specifies the category (book, food, medical_product or other) and origin (local or import) in the item definition. Indeed, it is safer to do so instead of extracting these information from the item description since the latter can be cumbersome and error-prone. For example, how to know that syrup, inhaler or ointment are medical products ? it would be necessary to list all of them.  

### Requirements ###

* Java 8
* Scala 2.11.11
* sbt 0.13.15

### How to build ###

The code is built with sbt.
To generate a package, execute: 
```bash
sbt dist
```

### How to run the application ###

After building the jar, unzip it and execute the bash script in the bin folder:
```bash
unzip ./target/universal/tax_calculator-1.0.zip
./tax_calculator-1.0/bin/tax_calculator
```

### How to run tests ###
There are 2 levels of testing: `UnitTest` and `IntegrationTest`.
To run tests, execute:
```bash
sbt test
```
To run a specific level of tests, execute:
```bash
sbt "test-only -- -n <TEST_LEVEL>"
```

### How is the system designed ###
The  following sequence diagram show the different components of the system as well as their main responsibilities.
![Alt text](design.png?raw=true "Design")
